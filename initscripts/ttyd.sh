#!/bin/bash
. /.env

if [ -z "$TTYD_PORT" ]; then
    TTYD_PORT="4100"
fi

TTYD_PATH="/usr/sbin/ttyd"
if [ ! -f "$TTYD_PATH" ]; then
    red='\033[0;31m'
    yellow='\033[0;33m'
    plain='\033[0m'

    err() {
        printf "${red}%s${plain}\n" "$*" >&2
    }

    info() {
        printf "${yellow}%s${plain}\n" "$*"
    }

    os_arch=""
    ## os_arch
    mach=$(uname -m)
    case "$mach" in
    amd64 | x86_64)
        os_arch="x86_64"
        ;;
    i386 | i686)
        os_arch="i686"
        ;;
    aarch64 | arm64)
        os_arch="aarch64"
        ;;
    *arm*)
        os_arch="arm"
        ;;
    s390x)
        os_arch="s390x"
        ;;
    mips)
        os_arch="mips"
        ;;
    mipsel | mipsle)
        os_arch="mipsel"
        ;;
    *)
        err "Unknown architecture: $uname"
        exit 1
        ;;
    esac

    info "> 安装ttyd"
    info "正在下载ttyd"
    wget -t 2 -T 10 -O $TTYD_PATH https://github.com/tsl0922/ttyd/releases/latest/download/ttyd.${os_arch} >/dev/null 2>&1
    if [[ $? != 0 ]]; then
        err "Release 下载失败，请检查本机能否连接 github.com"
        exit 1
    fi
fi

chmod +x $TTYD_PATH
exec "$TTYD_PATH" -6 -p $TTYD_PORT login
