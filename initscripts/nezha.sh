#!/bin/bash
. /.env

if [ -z "$NZ_ENV" ]; then
    exit 0
fi

NZ_AGENT_PATH="/opt/nezha/agent"
NZ_CFG_PATH="/data/nezha/agent"

if [ ! -f "$NZ_AGENT_PATH/nezha-agent" ]; then
    red='\033[0;31m'
    yellow='\033[0;33m'
    plain='\033[0m'

    err() {
        printf "${red}%s${plain}\n" "$*" >&2
    }

    info() {
        printf "${yellow}%s${plain}\n" "$*"
    }

    os_arch=""
    ## os_arch
    mach=$(uname -m)
    case "$mach" in
    amd64 | x86_64)
        os_arch="amd64"
        ;;
    i386 | i686)
        os_arch="386"
        ;;
    aarch64 | arm64)
        os_arch="arm64"
        ;;
    *arm*)
        os_arch="arm"
        ;;
    s390x)
        os_arch="s390x"
        ;;
    riscv64)
        os_arch="riscv64"
        ;;
    mips)
        os_arch="mips"
        ;;
    mipsel | mipsle)
        os_arch="mipsle"
        ;;
    *)
        err "Unknown architecture: $uname"
        exit 1
        ;;
    esac

    info "> 安装监控Agent"
    info "正在获取监控Agent版本号"

    # 哪吒监控文件夹
    mkdir -p $NZ_AGENT_PATH
    chmod 777 -R $NZ_AGENT_PATH

    info "正在下载监控端"
    wget -t 2 -T 10 -O agent.zip https://github.com/nezhahq/agent/releases/latest/download/nezha-agent_linux_${os_arch}.zip >/dev/null 2>&1
    if [[ $? != 0 ]]; then
        err "Release 下载失败，请检查本机能否连接 github.com"
        exit 1
    fi

    unzip -qo agent.zip &&
        mv nezha-agent $NZ_AGENT_PATH &&
        rm -rf agent.zip
fi

chmod +x $NZ_AGENT_PATH/nezha-agent
mkdir -p $NZ_CFG_PATH
exec "/usr/bin/env" $NZ_ENV "$NZ_AGENT_PATH/nezha-agent" "-c" "$NZ_CFG_PATH/config.yml"
