#!/bin/sh
echo -e "nameserver 8.8.8.8\nnameserver 8.8.4.4" >/etc/resolv.conf
export > /.env
mkdir -p /data/supervisor/

if [ ! -z "$DEBIAN_SSH_PASSWORD" ]; then
    echo "root:$DEBIAN_SSH_PASSWORD" | chpasswd root
fi

if [ ! -z "$DEBIAN_SSH_PORT" ]; then
    sed -i "s/^#\?Port.*/Port $DEBIAN_SSH_PORT/g" /etc/ssh/sshd_config
fi

if [ ! -f "/.initialized" ]; then
    if [ -f "/data/init.sh" ]; then
        /bin/bash /data/init.sh
    fi

    touch /.initialized
fi

exec "$@"