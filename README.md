# Debian Docker 镜像

使用 Debian 稳定分支构建的 Docker 镜像, 可以部署至容器云平台, 使其作为 VPS 使用

# 内建服务

- supervisor (一个类似 `systemd` 的守护进程)
- OpenSSH Server `22/TCP`
- ttyd 网页终端 `4100/TCP`
- 哪吒探针客户端 (需要设置`NEZHA_OPTS`环境变量)

# 环境变量

- `DEBIAN_SSH_PASSWORD` SSH 密码 (用户 root)
- `DEBIAN_SSH_PORT` SSH 容器内端口 (默认 22)
- `TTYD_PORT` ttyd 终端容器内端口 (默认 4100)
- `NEZHA_ENV` 哪吒探针安装参数

# 持久化数据保存

> 挂载点 `/data`

将数据存放在 `/data` 下, supervisor 配置文件存放在 `/data/supervisor/`下, 初始化脚本放在 `/data/init.sh`
