FROM debian:stable

ENV TZ="Asia/Shanghai"

COPY entrypoint.sh /entrypoint.sh
COPY initscripts /initscripts

RUN export DEBIAN_FRONTEND="noninteractive" && \
	apt-get update -y && apt-get install -y wget curl sudo nano vim git unzip net-tools iproute2 iputils-ping dialog openssh-server ca-certificates tzdata supervisor && \
	update-ca-certificates && \
	ln -sf /usr/share/zoneinfo/$TZ /etc/localtime && \
	dpkg-reconfigure tzdata && \
	sed -i 's/^#\?PasswordAuthentication.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config && \
	sed -i 's/^#\?PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config && \
	mkdir -p /run/sshd && \
	chmod +x /initscripts/* && \
	chmod +x /entrypoint.sh

COPY supervisord.conf /etc/supervisor/supervisord.conf
COPY conf.d /etc/supervisor/conf.d

WORKDIR /
VOLUME ["/data/"]

EXPOSE 22/tcp 4100/tcp

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/usr/bin/supervisord","-n","-c","/etc/supervisor/supervisord.conf"]
